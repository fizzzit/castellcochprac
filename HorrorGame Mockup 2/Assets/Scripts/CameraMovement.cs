using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraMovement : MonoBehaviour
{
    private float xRotation;
    [SerializeField] public float sensitivity = 90f;
    private Transform player;
    private Camera cam;
    public static bool cameraRotation = true;
    public static bool lookBackAbility = true;
    public Image interactibleUIDisplay;
    private Color OriginalColor;
    public LayerMask whatIsInteractible;

// Start is called before the first frame update
void Start()
    {
        //press ESCAPE to show the mouse again.
        cam = GetComponent<Camera>();
        player = transform.parent;
        cameraRotation = true;
        OriginalColor = interactibleUIDisplay.color;
        SetCursorState(false);
    }

    void Update()
    {
        CameraLookRotation(cameraRotation);
        Interacting(true);
        QuickLookBack(lookBackAbility);
    }

    public void SetSensivity()
    {
        sensitivity = GetComponent<SensitivityController>().SensSlider.value;
    }

    // giving the option to disable look rotation later on so we can have jumpscares
    private void CameraLookRotation(bool canLook)
    {
        if (!canLook)
            return;

        float verticalY = Input.GetAxis("Mouse Y") * Time.deltaTime * sensitivity; // Vertical Mouse Movement
        float horizontalX = Input.GetAxis("Mouse X") * Time.deltaTime * sensitivity; // Horizontal Mouse Movement

        xRotation -= verticalY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);
        transform.localEulerAngles = new Vector3(xRotation, 0f, 0f);

        // Set it to the player transform so that we can have this as an external script
        player.Rotate(Vector3.up * horizontalX);
    }

    private void QuickLookBack(bool canLookBack)
    {
        if (!canLookBack)
            return;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            cameraRotation = false;
            transform.Rotate(Vector3.up * -180);
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            transform.Rotate(Vector3.up * -180);
            cameraRotation = true;
        }
    }

    private void SetCursorState(bool enabled)
    {
        Cursor.visible = enabled; // will be modified for ui
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Interacting(bool interact)
    {
        if (interact == false)
            return;

        RaycastHit hit;
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 4, whatIsInteractible))
        {
            Transform interactedObject = hit.transform;
            if (hit.transform.GetComponent<Interactible>() != null)
            {
                GameManager.instance.isHighlighting = true;
                interactedObject.GetComponent<Interactible>().thisObject = true;
                interactibleUIDisplay.color = Color.white;
            }
            else if (hit.transform.GetComponent<Interactible>() == null)
            {
                GameManager.instance.isHighlighting = false;
                GameManager.instance.interactingObject = null;
                GameManager.instance.SetInteractionUI("", false);
                interactibleUIDisplay.color = OriginalColor;
            }
        }
        else
        {
            GameManager.instance.isHighlighting = false;
            GameManager.instance.interactingObject = null;
            GameManager.instance.SetInteractionUI("", false);
            interactibleUIDisplay.color = OriginalColor;
        }
    }

}
