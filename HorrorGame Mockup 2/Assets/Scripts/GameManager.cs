using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    [HideInInspector] public bool isHighlighting;

    public Action[] interactionTypes;
    [HideInInspector] public GameObject interactingObject;
    [SerializeField] private Transform holdPos;
    [SerializeField] private float maxPickupTimer;
    private float currentTime;
    private bool canInteract;

    [SerializeField] private GameObject lampObject;

    public GameObject interactionUI;
    public int scoreCounter;

    [SerializeField] private GameObject popupMessage;
    [SerializeField] private GameObject blankSlatePrefab;
    [SerializeField] private GameObject anthologyUIParent;
    [SerializeField] private TextMeshProUGUI anthologyCollectedText;
    [SerializeField] private int maxNotes;

    public int health;
    public GameObject deathScreen;

    public GameObject testSFXobj;

    public void adjustHealth(int value)
    {
        health -= value;
        if(health <= 0) {
            Time.timeScale = 0.25f;
            deathScreen.SetActive(true);
        }
    }

    void Start()
    {
        instance = this;
        isHighlighting = false;
        currentTime = maxPickupTimer;
        SetActions();
        Destroy(transform.GetChild(0).gameObject);

        for (int i = 0; i < maxNotes; i++)
        {
            Instantiate(blankSlatePrefab, anthologyUIParent.transform);
        }
    }

    private void SetActions()
    {
        interactionTypes = new Action[8];
        interactionTypes[0] = PickUpFunction;
        interactionTypes[1] = Interact;
        interactionTypes[2] = HideInItem;
        interactionTypes[3] = ToggleLamp;
        interactionTypes[4] = JumpscareInteraction;
        interactionTypes[5] = ToggleRoomLights;
        interactionTypes[6] = EnterDoor;
        interactionTypes[7] = PlayPiano;
    }

    public void CallAction(int ID, GameObject setInteractionObject)
    {
        interactingObject = setInteractionObject;
        if (interactingObject != null)
        {
            interactionTypes[ID]();
        }
    }

    private void Update()
    {
        if (interactingObject != null)
        {
            if (Input.GetKeyDown(KeyCode.E) && interactingObject.GetComponent<Interactible>().beinginteractedWith == true && canInteract)
            {
                StopAllCoroutines();
                interactingObject.GetComponent<Interactible>().beinginteractedWith = false;
                //StartCoroutine(HoldItem(interactingObject.transform, holdPos, 10));
                StartCoroutine(PutItemBack(interactingObject.transform, interactingObject.GetComponent<Interactible>().originalPosition, interactingObject.GetComponent<Interactible>().originalRotation, 10));
            }
        }

        if (currentTime < maxPickupTimer)
        {
            currentTime += Time.deltaTime;
            canInteract = false;
        }
        if (currentTime >= maxPickupTimer)
        {
            canInteract = true;
        }
    }
    // jumpscare, opening/closing doors, pick up item into inventory, hide in something, toggle environment

    #region Pick up and look at object
    private void PickUpFunction()
    {
        var item = interactingObject.transform;

        StopAllCoroutines();
        StartCoroutine(HoldItem(item, holdPos, 10));
        Debug.Log(interactingObject.name.ToString());
    }

    IEnumerator HoldItem(Transform itemToMove, Transform position, float moveSpeed)
    {
        currentTime = 0;
        PlayerMovement.isMoving = false;
        itemToMove.GetComponent<Interactible>().beinginteractedWith = true;
        CameraMovement.cameraRotation = false;
        while (itemToMove.position != position.position)
        {
            itemToMove.position = Vector3.MoveTowards(itemToMove.position, position.position, Time.deltaTime * moveSpeed);
            yield return null;
        }
    }

    IEnumerator PutItemBack(Transform itemToMove, Vector3 position, Quaternion rotation, float moveSpeed)
    {
        PlayerMovement.isMoving = true;
        interactingObject.GetComponent<Interactible>().beinginteractedWith = false;
        CameraMovement.cameraRotation = true;
        while (itemToMove.position != position && itemToMove.rotation != rotation)
        {
            itemToMove.position = Vector3.MoveTowards(itemToMove.position, position, Time.deltaTime * moveSpeed);
            itemToMove.rotation = Quaternion.Slerp(itemToMove.rotation, rotation, Time.deltaTime * moveSpeed);
            yield return null;
        }
        itemToMove.rotation = interactingObject.GetComponent<Interactible>().originalRotation;
        currentTime = 0;
    }
    #endregion

    private void Interact() // Harvey's inventory system
    {
        if (interactingObject.GetComponent<Interactible>().anthologyPrefab != null)
        {
            int anthologyCounter = 0;
            var interactScript = interactingObject.GetComponent<Interactible>();
            AudioSource audioSource = interactingObject.GetComponent<AudioSource>();

            anthologyUIParent.transform.GetChild(interactScript.letter.ID - 1).GetComponent<LetterLoader>().SetLetter(interactScript.letter);
            
            audioSource.clip = interactScript.GetAudioClip();
            audioSource.Play();

            for (int i = 0; i < anthologyUIParent.transform.childCount; i++)
            {
                if (anthologyUIParent.transform.GetChild(i).GetComponent<LetterLoader>().letter != null)
                    anthologyCounter++;
                
            }
            if (anthologyCounter == 2)
            {
                Debug.Log("Exactly 2 letters collected");
                anthologyCollectedText.text = "We meet again, my love.";
            }

            else if (anthologyCounter == 4)
            {
                Debug.Log("Exactly 4 letters collected");
                anthologyCollectedText.text = "You weren't here for me when I needed you.";
            }
            else if (anthologyCounter == 6)
            {
                Debug.Log("Exactly 6 letters collected");
                anthologyCollectedText.text = "Am I how you remembered?";
            }
            else if (anthologyCounter >= 8)
            {
                Debug.Log("YAY");
                anthologyCollectedText.text = "I guess you know what happened to me, I give up.";

                // GAME WIN CONDITION
                Debug.Log("The player has won the game");
            }
            else
            {
                anthologyCollectedText.text = "Added Anthology: " + "(" + anthologyCounter.ToString() + " / 8)";
            }

            StartCoroutine(AddToAnthology(5));
        }

        interactingObject.gameObject.SetActive(false);
    }

    private void PlayPiano()
    {
        AudioSource audio = interactingObject.GetComponent<AudioSource>();
        AudioSource bgm = GameManager.instance.GetComponentInChildren<AudioSource>();
        if (audio.isPlaying)
        {
            bgm.Play();
            audio.Stop();
        }
        else
        {
            bgm.Stop();
            audio.Play();
        }
        
    }

    IEnumerator AddToAnthology(int seconds)
    {
        // Begin Popup message
        //TextMeshProUGUI popupText = popupMessage.GetComponent<TextMeshProUGUI>();

        Debug.Log("Displaying 'Adding to Anthology Message'");
        popupMessage.SetActive(true);
        yield return new WaitForSeconds(seconds);
        Debug.Log("Fading message out");
        popupMessage.SetActive(false);

        // End Popup message
    }

    private void HideInItem()
    {
        Debug.Log("Hiding In Item");
    }

    private void ToggleLamp() // Made By Calum
    {
        lampObject.SetActive(true);
        Destroy(interactingObject);
        //Add points here
        ScoreCounter(1);
        Invoke("DisableLampLight", 30.0f);
    }

    private void DisableLampLight()
    {
        lampObject.SetActive(false);
    }

    private void JumpscareInteraction()
    {
        Debug.Log("Jumpscare");
        StopAllCoroutines();
        StartCoroutine(CallJumpscare());
    }

    IEnumerator CallJumpscare()
    {
        // jumpscare happens here
        PlayerMovement.isMoving = false;
        CameraMovement.cameraRotation = false;
        yield return new WaitForSeconds(1);
        PlayerMovement.isMoving = true;
        CameraMovement.cameraRotation = true;
        // finish jumpscare
    }

    private void ToggleRoomLights()
    {
        if(interactingObject.transform.GetChild(0).GetChild(0).gameObject.activeSelf == true)
        {
            for (int i = 0; i < interactingObject.transform.GetChild(0).childCount; i++)
            {
                interactingObject.transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
            }
        }
        else
        {
            for (int i = 0; i < interactingObject.transform.GetChild(0).childCount; i++)
            {
                interactingObject.transform.GetChild(0).GetChild(i).gameObject.SetActive(true);
            }
        }
    }

    private void EnterDoor()
    {
        // apply animaiton here
        SceneManager.LoadScene(interactingObject.GetComponent<Interactible>().sceneName);
    }

    private void HandleUI()
    {

    }
    
    public void SetInteractionUI(string textToDisplay, bool active)
    {
        interactionUI.SetActive(active);
        if(textToDisplay != null)
        {
            interactionUI.GetComponent<TextMeshProUGUI>().text = textToDisplay;
        }
    }

    public void ScoreCounter(int amount)
    {
        scoreCounter += amount;
        if(scoreCounter >= 7)
        {
            Debug.Log("You win");
        }
    }
}

public enum FunctionTypes
{
    PickUp,
    Interact,
    Hide,
    ToggleLights,
    Jumpscare,
    adjustRoomLights,
    enterDoor,
    PlayPiano
}