using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(NavMeshAgent))]
public class EnemyMovement : MonoBehaviour
{
    private NavMeshAgent agent;
    private Transform viewPortContainer;
    [SerializeField] private Transform newPosition;
    [SerializeField] private LayerMask whatIsPlayer;
    public EnemyStates currentState = EnemyStates.patroling;

    private bool[] hitChecks;
    private int raycastHitCount;
    public bool foundPlayer = false;

    private Transform patrolPoints;
    private Transform lastSeenPlayerPos;
    private Transform player;

    private int patrolID = 0;

    public float currentChaseTime;
    [SerializeField] private float maxChaseTime;

    public GameObject chaseVignette;
    [SerializeField] private AudioSource footsteps;
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        viewPortContainer = transform.GetChild(0).transform;
        patrolPoints = transform.GetChild(1).transform;

        hitChecks = new bool[viewPortContainer.childCount];

        patrolID = newPosition.GetSiblingIndex();
        agent.destination = newPosition.position;
        patrolPoints.parent = null;

        GameObject newPlayerPos = new GameObject("LastSeenPlayerPos");
        lastSeenPlayerPos = newPlayerPos.transform;

        currentChaseTime = maxChaseTime;
    }

    void Update()
    {
        if (raycastHitCount >= 3)
        {
            currentChaseTime = 0;
            foundPlayer = true;
            chaseVignette.SetActive(true);
        }

        if (currentChaseTime < maxChaseTime && raycastHitCount < 3)
        {
            lastSeenPlayerPos.position = PlayerMovement.player.position;
            currentState = EnemyStates.chasing;
            agent.destination = lastSeenPlayerPos.position;
            footsteps.pitch = 1.25f;
            agent.speed = 7;

            currentChaseTime += 1 * Time.deltaTime;
        }
        else if (currentChaseTime >= maxChaseTime && raycastHitCount < 3 && foundPlayer)
        {
            currentState = EnemyStates.patroling;
            footsteps.pitch = 0.5f;
            agent.speed = 4;

            agent.destination = patrolPoints.GetChild(patrolID).position;
            chaseVignette.SetActive(false);
            foundPlayer = false;
        }
    }

    private void FixedUpdate()
    {
        Vision();
    }
    private void Vision()
    {
        if (hitChecks.Length <= 0)
            return;

        for (int i = 0; i < viewPortContainer.childCount; i++)
        {
            RaycastHit hit;
            Transform currentChild = viewPortContainer.GetChild(i);
            var forward = currentChild.TransformDirection(Vector3.forward);
            if (Physics.Raycast(currentChild.position, forward, out hit, 20, whatIsPlayer))
            {
                // has hit something
                if (hit.transform.gameObject.CompareTag("Player"))
                {
                    Debug.DrawRay(currentChild.position, forward * hit.distance, Color.green);
                    hitChecks[i] = true;
                }
                else
                {
                    Debug.DrawRay(currentChild.position, forward * hit.distance, Color.red);
                    hitChecks[i] = false;
                }
            }
            else
            {
                // hasn't hit anything
                Debug.DrawRay(currentChild.position, forward * 20, Color.red);
                hitChecks[i] = false;
            }

            raycastHitCount = hitChecks.Sum(x => x ? 1 : 0);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Patrol") && currentState == EnemyStates.patroling)
        {
            if (other.transform.parent != patrolPoints)
                return;

            patrolID = other.transform.GetSiblingIndex(); patrolID++;
            if (patrolID > patrolPoints.childCount - 1)
                patrolID = 0;

            agent.destination = patrolPoints.GetChild(patrolID).position;
        }
    }
}

public enum EnemyStates
{
    patroling,
    chasing
}