using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseManager : MonoBehaviour
{
    private bool state;
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private GameObject opitions;
    void Update()
    {
        // this has been set to tab because of unity. we will change it before building
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(state)
            {
                Time.timeScale = 0.25f;
                CameraMovement.cameraRotation = false;
                PlayerMovement.isMoving = false;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.Confined;

                pauseMenu.SetActive(true);
                opitions.SetActive(false);

                state = false;
            }
            else
            {
                Time.timeScale = 1f;
                CameraMovement.cameraRotation = true;
                PlayerMovement.isMoving = true;
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;

                pauseMenu.SetActive(false);

                state = true;
            }
        }
    }
}
