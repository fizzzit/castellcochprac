using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpTrigger : MonoBehaviour
{
    public AudioSource Scream;
    public AudioSource SpookyMusic;
    public AudioSource footsteps;
    private GameObject Player;
    public GameObject JumpView;
    public GameObject effects;


    private void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
     
    }
    
    public void OnTriggerEnter(Collider otherObject)
    {
        if(otherObject.gameObject.CompareTag("Player"))
        {
            Scream.Play();
            SpookyMusic.Play();
            JumpView.SetActive(true);
            Player.SetActive(false);
            effects.SetActive(true);

            footsteps.volume = 0;
            GameManager.instance.adjustHealth(1);

            StartCoroutine(EndJump());
        }
    }

    IEnumerator EndJump()
    {
        yield return new WaitForSeconds(1.5f);

        GetComponent<Collider>().enabled = false;
        Player.SetActive(true);
        JumpView.SetActive(false);
        effects.SetActive(false);
        
        footsteps.volume = 1;

        yield return new WaitForSeconds(3f);
        GetComponent<Collider>().enabled = true;
    }
}
