using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactible : MonoBehaviour
{
    [HideInInspector] public bool thisObject;
    [HideInInspector] public bool beinginteractedWith = false;
    public FunctionTypes functions;
    [HideInInspector] public Vector3 originalPosition;
    [HideInInspector] public Quaternion originalRotation;
    public string sceneName = "N/A";
    public string eToInteract = "E To Interact";
    public GameObject anthologyPrefab;
    public Letter letter;
    public AudioClip audioClip;
    
    private void Start()
    {
        originalPosition = transform.position;
        originalRotation = transform.rotation;
        gameObject.layer = LayerMask.NameToLayer("Interactible");
    }

    void Update()
    {
        if (GameManager.instance.isHighlighting == true && thisObject == true)
        {
            if(eToInteract.ToCharArray().Length > 0)
            {
                GameManager.instance.SetInteractionUI(eToInteract, true);
            }
            else
            {
                GameManager.instance.SetInteractionUI("", false);
            }
            
            //Debug.Log("Working : " + gameObject.name.ToString());
            if (Input.GetKeyUp(KeyCode.E) && beinginteractedWith == false)
            {
                if (transform.position == originalPosition && transform.rotation == originalRotation)
                {
                    SelectFunction();
                }
            }
        }
        else if (GameManager.instance.isHighlighting == false)
        {
            thisObject = false;
        }

        if(functions == FunctionTypes.adjustRoomLights)
        {
            
        }

        if (beinginteractedWith == true)
        {
            float x = Input.GetAxis("Mouse X") * 5;
            float y = Input.GetAxis("Mouse Y") * 5;
            transform.Rotate(-Vector3.up * x, Space.World);
            transform.Rotate(-Vector3.forward * y, Space.World);
        }
    }

    private void SelectFunction()
    {
        GameManager.instance.CallAction(((int)functions), this.gameObject);
    }

    public AudioClip GetAudioClip()
    {
        return audioClip;
    }

    

}
