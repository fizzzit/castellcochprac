using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private float forward;
    private float sideways;
    private bool crouchState;
    private bool hitCeilingFront, hitCeilingBack, ceilingLeft, ceilingRight;
    private bool notUnderCeiling;
    public float speed;

    private Rigidbody rb;
    private CapsuleCollider col;

    private Transform positions;
    private Transform cameraTransform;

    public LayerMask whatIsCeiling;

    public static bool isMoving;
    public static Transform player;

    public GameObject footsteps;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        player = this.transform;
        cameraTransform = transform.GetChild(0);
        positions = transform.GetChild(1);

        isMoving = true;
    }

    private void Update()
    {
        if (col != null)
            Crouch();

        Sprint();
    }

    void FixedUpdate()
    {
        if (rb == null)
            return;

        MovePlayer(isMoving);
        Sprint();
        RaycastCeilingChecks();
    }

    private void MovePlayer(bool canMove)
    {
        if (canMove == false)
            return;

        forward = Input.GetAxis("Vertical") * speed;
        sideways = Input.GetAxis("Horizontal") * speed;

        rb.velocity = (transform.forward * forward) + (transform.right * sideways) + (transform.up * rb.velocity.y);
        if (forward != 0 || sideways != 0)
        {
            footsteps.SetActive(true);
        }
        else
        {
            footsteps.SetActive(false);
        }
    }

    private void Crouch()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            col.height = 1f;
            crouchState = true;

            if (speed > 2)
                footsteps.GetComponent<AudioSource>().pitch = 1.2f;
            else
                footsteps.GetComponent<AudioSource>().pitch = 0.5f;
        }
        else if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            if (notUnderCeiling == true)
            {
                footsteps.GetComponent<AudioSource>().pitch = 0.8f;
                col.height = 2;
            }
            else
                footsteps.GetComponent<AudioSource>().pitch = 0.5f;

            crouchState = false;
        }

        if (notUnderCeiling && !crouchState && col.height < 2)
        {
            if (speed > 2)
                footsteps.GetComponent<AudioSource>().pitch = 1.2f;
            else
                footsteps.GetComponent<AudioSource>().pitch = 0.8f;

            col.height = 2;
        }
    }

    private void Sprint()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            speed = 6;
            if (crouchState && notUnderCeiling)
            {
                footsteps.GetComponent<AudioSource>().pitch = 0.8f;
            }
            else
            {
                footsteps.GetComponent<AudioSource>().pitch = 1.2f;
            }
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            footsteps.GetComponent<AudioSource>().pitch = 0.8f;
            speed = 2;
        }
    }

    private void RaycastCeilingChecks()
    {
        for (int i = 0; i < positions.childCount; i++)
        {
            RaycastHit hit;
            if (Physics.Raycast(positions.GetChild(i).position, transform.up, out hit, 1, whatIsCeiling))
            {
                Debug.DrawRay(positions.GetChild(i).position, transform.up * hit.distance, Color.white);
                if (i == 0)
                {
                    hitCeilingFront = true;
                }
                else if (i == 1)
                {
                    hitCeilingBack = true;
                }
                else if (i == 2)
                {
                    ceilingRight = true;
                }
                else if (i == 3)
                {
                    ceilingLeft = true;
                }
            }
            else
            {
                Debug.DrawRay(positions.GetChild(i).position, transform.up * 1, Color.white);
                if (i == 0)
                {
                    hitCeilingFront = false;
                }
                else if (i == 1)
                {
                    hitCeilingBack = false;
                }
                else if (i == 2)
                {
                    ceilingRight = false;
                }
                else if (i == 3)
                {
                    ceilingLeft = false;
                }
            }
        }

        if ((hitCeilingFront == false && hitCeilingBack == false && ceilingRight == false && ceilingLeft == false))
            notUnderCeiling = true;
        else
            notUnderCeiling = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.CompareTag("RoomTriggerBox"))
        {
            MenuManager.instance.SetRoomName(other.name);
        }
    }
}
