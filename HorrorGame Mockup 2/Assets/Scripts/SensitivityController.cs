using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SensitivityController : MonoBehaviour
{

    public Slider SensSlider;
    public TMP_Text SensText;

    [SerializeField] public float UserSensitivity;

    // Update is called once per frame
    void Update()
    {
        SensText.text = "Sensitivity: " + SensSlider.value;
        UserSensitivity = SensSlider.value;
    }
}
