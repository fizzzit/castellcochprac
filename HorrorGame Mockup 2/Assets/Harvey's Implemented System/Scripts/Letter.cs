using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[CreateAssetMenu(fileName = "Letter", menuName = "Letters")]
public class Letter : ScriptableObject
{
    // General Attributes
    public int ID;
    public string Author;
    public TMP_FontAsset font;

    // Letter Attributes
    [TextArea]
    public string letterText;
}
