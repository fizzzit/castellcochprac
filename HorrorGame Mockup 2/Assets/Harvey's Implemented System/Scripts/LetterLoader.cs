using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LetterLoader : MonoBehaviour
{
    public Letter letter;

    // Visual References
    public TextMeshProUGUI letterTextField;
    public TextMeshProUGUI authorTextField;
    public TMP_FontAsset font;

    public void SetLetter(Letter temp)
    {
        letter = temp;
        letterTextField.text = letter.letterText;
        letterTextField.font = font;
        authorTextField.text = "Author: " + letter.Author;
    }
}
