﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Letter::.ctor()
extern void Letter__ctor_mAD96D012C22B8DF35C6D036250A8F947A827DAB7 (void);
// 0x00000002 System.Void LetterLoader::SetLetter(Letter)
extern void LetterLoader_SetLetter_m8888CCD20911156DE5BB5292518EB9D30C176EB1 (void);
// 0x00000003 System.Void LetterLoader::.ctor()
extern void LetterLoader__ctor_mC632C041813FE27B9C3FAB0EBDA3AE9D3E029B89 (void);
// 0x00000004 System.Void CameraMovement::Start()
extern void CameraMovement_Start_mCBBF91D02D609BDE618205B77F07B1FFD53C0A41 (void);
// 0x00000005 System.Void CameraMovement::Update()
extern void CameraMovement_Update_m02A5D16421ED8C060C06A72144FEA9250C55CE2B (void);
// 0x00000006 System.Void CameraMovement::SetSensivity()
extern void CameraMovement_SetSensivity_m0FBA41D5DC333A5B60BFD5CC290DA6F0B81DFA42 (void);
// 0x00000007 System.Void CameraMovement::CameraLookRotation(System.Boolean)
extern void CameraMovement_CameraLookRotation_m68A00AC80F30083F65C63BF47119418A64A7F8E7 (void);
// 0x00000008 System.Void CameraMovement::QuickLookBack(System.Boolean)
extern void CameraMovement_QuickLookBack_m561AEE70EB4F49EDAEBC67AC324848E1D90B4384 (void);
// 0x00000009 System.Void CameraMovement::SetCursorState(System.Boolean)
extern void CameraMovement_SetCursorState_m1008B826218BB7C58CD7C23D6D750B124ED1A8B7 (void);
// 0x0000000A System.Void CameraMovement::Interacting(System.Boolean)
extern void CameraMovement_Interacting_mA6DB1B5CD7FDBA065DFE1E2657913355BE13D8CB (void);
// 0x0000000B System.Void CameraMovement::.ctor()
extern void CameraMovement__ctor_mD0F05084B2475AA8C0A35AFB6E4C88D0D6ACEAAA (void);
// 0x0000000C System.Void CameraMovement::.cctor()
extern void CameraMovement__cctor_m16070BB162FA3D85BBBFF01B728D48115758A28B (void);
// 0x0000000D System.Void EnemyMovement::Start()
extern void EnemyMovement_Start_mB666BB660A59344BCAE9FC29700BDE66E1923C8F (void);
// 0x0000000E System.Void EnemyMovement::Update()
extern void EnemyMovement_Update_mDC79756DF8371F3CEDD9D33F9D7E0734ABCD141C (void);
// 0x0000000F System.Void EnemyMovement::FixedUpdate()
extern void EnemyMovement_FixedUpdate_m5986E4E1AAFE4FE79575DE73332CE5668D30A89C (void);
// 0x00000010 System.Void EnemyMovement::Vision()
extern void EnemyMovement_Vision_mDE352EEACF474A852C3C3BD7E61FA82A163D341E (void);
// 0x00000011 System.Void EnemyMovement::OnTriggerEnter(UnityEngine.Collider)
extern void EnemyMovement_OnTriggerEnter_mEA3DD86A39F4BC94349C4F54DD252253B5D79C3F (void);
// 0x00000012 System.Void EnemyMovement::.ctor()
extern void EnemyMovement__ctor_mFD697CA9B04655253B087314FE67A3B388E55EFF (void);
// 0x00000013 System.Void EnemyMovement/<>c::.cctor()
extern void U3CU3Ec__cctor_m83BFA3B9CDB16819CA6A68533EC476613E6E4111 (void);
// 0x00000014 System.Void EnemyMovement/<>c::.ctor()
extern void U3CU3Ec__ctor_m1EBDD2D597505E663934614FDF24DAC45C75515C (void);
// 0x00000015 System.Int32 EnemyMovement/<>c::<Vision>b__19_0(System.Boolean)
extern void U3CU3Ec_U3CVisionU3Eb__19_0_m7C4A64AA0BF79D1DA3F8E4844F83EDC95A64C92E (void);
// 0x00000016 System.Void GameManager::adjustHealth(System.Int32)
extern void GameManager_adjustHealth_m038AB8D4112F7AE16280E4DD404E510BFE5D4144 (void);
// 0x00000017 System.Void GameManager::Start()
extern void GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E (void);
// 0x00000018 System.Void GameManager::SetActions()
extern void GameManager_SetActions_m98BE50DA36A667923298F5468BF3A6F344240DCA (void);
// 0x00000019 System.Void GameManager::CallAction(System.Int32,UnityEngine.GameObject)
extern void GameManager_CallAction_mA8016A2AD19D1696066B5003E00D497C9605FBC5 (void);
// 0x0000001A System.Void GameManager::Update()
extern void GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1 (void);
// 0x0000001B System.Void GameManager::PickUpFunction()
extern void GameManager_PickUpFunction_m35D731F93625BE2D10485AC4787071F621F8F14A (void);
// 0x0000001C System.Collections.IEnumerator GameManager::HoldItem(UnityEngine.Transform,UnityEngine.Transform,System.Single)
extern void GameManager_HoldItem_m587391581B39D91DAE01A7AFEA45A70243C9EC4D (void);
// 0x0000001D System.Collections.IEnumerator GameManager::PutItemBack(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion,System.Single)
extern void GameManager_PutItemBack_m67BC44FF52522AB0B8D1F76831022C1D844F1C6B (void);
// 0x0000001E System.Void GameManager::Interact()
extern void GameManager_Interact_mC745154B5424A165E17008ACC5837FBB411A7B3B (void);
// 0x0000001F System.Void GameManager::PlayPiano()
extern void GameManager_PlayPiano_mA380B3243C66CD2FFF2D47CAF5B56C442AC1D36C (void);
// 0x00000020 System.Collections.IEnumerator GameManager::AddToAnthology(System.Int32)
extern void GameManager_AddToAnthology_mD755AE158B66AB6764FCB6E0320B6D773C8F8A02 (void);
// 0x00000021 System.Void GameManager::HideInItem()
extern void GameManager_HideInItem_m8A5B7754FCF2A2FFBF88C86C5C418DE2879045C8 (void);
// 0x00000022 System.Void GameManager::ToggleLamp()
extern void GameManager_ToggleLamp_m25A9EB49458FDD99E191AEB7323A324D79DB3B1E (void);
// 0x00000023 System.Void GameManager::DisableLampLight()
extern void GameManager_DisableLampLight_m89029C03CBAC60813CB8293FCCEF43E4BD0AD3E1 (void);
// 0x00000024 System.Void GameManager::JumpscareInteraction()
extern void GameManager_JumpscareInteraction_m28AD084959417E11FFC07DB517E2EC337492FA7A (void);
// 0x00000025 System.Collections.IEnumerator GameManager::CallJumpscare()
extern void GameManager_CallJumpscare_m8DBAE3AFB1C7E3A95AE1F56562D4607A3DD587FC (void);
// 0x00000026 System.Void GameManager::ToggleRoomLights()
extern void GameManager_ToggleRoomLights_m8C4A2FFD276ABECD3BD5966ADFD6E825768C8E71 (void);
// 0x00000027 System.Void GameManager::EnterDoor()
extern void GameManager_EnterDoor_m8789CFFF0C99A2F5310776D91CD8B613168E85DF (void);
// 0x00000028 System.Void GameManager::HandleUI()
extern void GameManager_HandleUI_m7DD55CC987CD2122AA47673335C0D721CE122E5C (void);
// 0x00000029 System.Void GameManager::SetInteractionUI(System.String,System.Boolean)
extern void GameManager_SetInteractionUI_mAA4EB27271424B134062F7EE5AB5C502D7EFF4A4 (void);
// 0x0000002A System.Void GameManager::ScoreCounter(System.Int32)
extern void GameManager_ScoreCounter_mB802BBC6D093A4A13C7E71B9DAEB426F92C6B176 (void);
// 0x0000002B System.Void GameManager::.ctor()
extern void GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D (void);
// 0x0000002C System.Void GameManager/<HoldItem>d__25::.ctor(System.Int32)
extern void U3CHoldItemU3Ed__25__ctor_m89F81FF8A4D3A0AD02A20E5EFC33E105AD97CF97 (void);
// 0x0000002D System.Void GameManager/<HoldItem>d__25::System.IDisposable.Dispose()
extern void U3CHoldItemU3Ed__25_System_IDisposable_Dispose_m97BFB6D6E12339704505728CE2E771F929DAF1F6 (void);
// 0x0000002E System.Boolean GameManager/<HoldItem>d__25::MoveNext()
extern void U3CHoldItemU3Ed__25_MoveNext_m64842F109F3E5CB2FC95E90FC8A3348C9CEC0B23 (void);
// 0x0000002F System.Object GameManager/<HoldItem>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CHoldItemU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD285725BA9E3166E630379865ACD8FCC7F11BE8B (void);
// 0x00000030 System.Void GameManager/<HoldItem>d__25::System.Collections.IEnumerator.Reset()
extern void U3CHoldItemU3Ed__25_System_Collections_IEnumerator_Reset_m69E913C1D3551E1352C4C4868717843372C83416 (void);
// 0x00000031 System.Object GameManager/<HoldItem>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CHoldItemU3Ed__25_System_Collections_IEnumerator_get_Current_m9D8FB0DA25EAF4AEFE9945C9C921CDAE39C63FCD (void);
// 0x00000032 System.Void GameManager/<PutItemBack>d__26::.ctor(System.Int32)
extern void U3CPutItemBackU3Ed__26__ctor_m8E1596EC73811FF47912D93AB594FA61E5AC6A9F (void);
// 0x00000033 System.Void GameManager/<PutItemBack>d__26::System.IDisposable.Dispose()
extern void U3CPutItemBackU3Ed__26_System_IDisposable_Dispose_m1CC1DFD466C725508CB90A4E25602067C0017035 (void);
// 0x00000034 System.Boolean GameManager/<PutItemBack>d__26::MoveNext()
extern void U3CPutItemBackU3Ed__26_MoveNext_m11A53FD679A7D3A898CE9434330D47B4C8BCAB66 (void);
// 0x00000035 System.Object GameManager/<PutItemBack>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPutItemBackU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBB881D70FABD4D44B103EAECAAEC1777936FBC5F (void);
// 0x00000036 System.Void GameManager/<PutItemBack>d__26::System.Collections.IEnumerator.Reset()
extern void U3CPutItemBackU3Ed__26_System_Collections_IEnumerator_Reset_m2381396556CD6BD919AC2ACFBD124A562FFC3F33 (void);
// 0x00000037 System.Object GameManager/<PutItemBack>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CPutItemBackU3Ed__26_System_Collections_IEnumerator_get_Current_m5E1352C3F93E1CEE02C83D58C1652FA90AAE1D40 (void);
// 0x00000038 System.Void GameManager/<AddToAnthology>d__29::.ctor(System.Int32)
extern void U3CAddToAnthologyU3Ed__29__ctor_mE00A39568E7B11F4CDAE27497D178B22CAEEAACD (void);
// 0x00000039 System.Void GameManager/<AddToAnthology>d__29::System.IDisposable.Dispose()
extern void U3CAddToAnthologyU3Ed__29_System_IDisposable_Dispose_m62971ECBB5A34D558CB0169E7A61A331CC0063D4 (void);
// 0x0000003A System.Boolean GameManager/<AddToAnthology>d__29::MoveNext()
extern void U3CAddToAnthologyU3Ed__29_MoveNext_m996D2A46004239BCF88C14E0679AF37D3F7A69F5 (void);
// 0x0000003B System.Object GameManager/<AddToAnthology>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAddToAnthologyU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m01D148E1C6FDBC7D26E2E678164C2CC52DDF9864 (void);
// 0x0000003C System.Void GameManager/<AddToAnthology>d__29::System.Collections.IEnumerator.Reset()
extern void U3CAddToAnthologyU3Ed__29_System_Collections_IEnumerator_Reset_mBB794F7C08E4CC7F6C0F0BC91EB48299CE2BBAF1 (void);
// 0x0000003D System.Object GameManager/<AddToAnthology>d__29::System.Collections.IEnumerator.get_Current()
extern void U3CAddToAnthologyU3Ed__29_System_Collections_IEnumerator_get_Current_mD0EA287B06953CC7A85BAB8F560DF8FEBBB67979 (void);
// 0x0000003E System.Void GameManager/<CallJumpscare>d__34::.ctor(System.Int32)
extern void U3CCallJumpscareU3Ed__34__ctor_mF174E3341A734D551157AAD46F452BE8BC05C24E (void);
// 0x0000003F System.Void GameManager/<CallJumpscare>d__34::System.IDisposable.Dispose()
extern void U3CCallJumpscareU3Ed__34_System_IDisposable_Dispose_m312982AC4ACB7E28063DF29E743AB2384C22152F (void);
// 0x00000040 System.Boolean GameManager/<CallJumpscare>d__34::MoveNext()
extern void U3CCallJumpscareU3Ed__34_MoveNext_mBBD45474A1C70902F9048CB23E5C1012353A5838 (void);
// 0x00000041 System.Object GameManager/<CallJumpscare>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCallJumpscareU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m47B314BD56C41DE7137B474CF6F328818691DA20 (void);
// 0x00000042 System.Void GameManager/<CallJumpscare>d__34::System.Collections.IEnumerator.Reset()
extern void U3CCallJumpscareU3Ed__34_System_Collections_IEnumerator_Reset_mDAFF898253879A4405C9FC76B0225C2A55BBD809 (void);
// 0x00000043 System.Object GameManager/<CallJumpscare>d__34::System.Collections.IEnumerator.get_Current()
extern void U3CCallJumpscareU3Ed__34_System_Collections_IEnumerator_get_Current_m0719B040B8A8672404A13FBA7C78E35B69C3928A (void);
// 0x00000044 System.Void Interactible::Start()
extern void Interactible_Start_m8C73B715550A3737AFCA1AE0F93F8B409F2731A0 (void);
// 0x00000045 System.Void Interactible::Update()
extern void Interactible_Update_m7539B2C9A1D4262E7439EAB6F405870489E3922A (void);
// 0x00000046 System.Void Interactible::SelectFunction()
extern void Interactible_SelectFunction_m0C2C7D7A60026C7B4DD055A3AA265A345BC0B532 (void);
// 0x00000047 UnityEngine.AudioClip Interactible::GetAudioClip()
extern void Interactible_GetAudioClip_m5D18C4DDCCEC81499CC48D3EEBF03E00546DC25E (void);
// 0x00000048 System.Void Interactible::.ctor()
extern void Interactible__ctor_m1499DC3597B9DA9A50541636476CF3DF61A052A6 (void);
// 0x00000049 System.Void JumpTrigger::Start()
extern void JumpTrigger_Start_mE3C8C936CBD174EC4C690E39401C68967001B548 (void);
// 0x0000004A System.Void JumpTrigger::OnTriggerEnter(UnityEngine.Collider)
extern void JumpTrigger_OnTriggerEnter_m30E20C1E1003BFFF7DC8EFF4F6E7A37F1D83EE1F (void);
// 0x0000004B System.Collections.IEnumerator JumpTrigger::EndJump()
extern void JumpTrigger_EndJump_m09AD09B655B13D6AAD1D5DB254C5DF3CB4505C79 (void);
// 0x0000004C System.Void JumpTrigger::.ctor()
extern void JumpTrigger__ctor_mA3CBA826B955A5F7ABD107AE87B82C02AC1E6614 (void);
// 0x0000004D System.Void JumpTrigger/<EndJump>d__8::.ctor(System.Int32)
extern void U3CEndJumpU3Ed__8__ctor_mBFA9DAB81A880A4C12EBC3DD852C9479D66DB0F5 (void);
// 0x0000004E System.Void JumpTrigger/<EndJump>d__8::System.IDisposable.Dispose()
extern void U3CEndJumpU3Ed__8_System_IDisposable_Dispose_m0FC917E3F6349A23718B9C6CCB439A5468530B81 (void);
// 0x0000004F System.Boolean JumpTrigger/<EndJump>d__8::MoveNext()
extern void U3CEndJumpU3Ed__8_MoveNext_m80B4AF45783CA3399C31E309D84B0C1E2435BAD3 (void);
// 0x00000050 System.Object JumpTrigger/<EndJump>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEndJumpU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m642084C8A35B9E4D690D79941AF724BAE0C24228 (void);
// 0x00000051 System.Void JumpTrigger/<EndJump>d__8::System.Collections.IEnumerator.Reset()
extern void U3CEndJumpU3Ed__8_System_Collections_IEnumerator_Reset_mDA18F083097A989CEC48EF1DC9FB5213B460E75B (void);
// 0x00000052 System.Object JumpTrigger/<EndJump>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CEndJumpU3Ed__8_System_Collections_IEnumerator_get_Current_mEC8C80BD4CF225FE441CD1889401A1AE1B123A0B (void);
// 0x00000053 System.Void MenuManager::Start()
extern void MenuManager_Start_m4E4A5EF33C27448D7F34FD29B93589635F1B2EE2 (void);
// 0x00000054 System.Void MenuManager::SetRoomName(System.String)
extern void MenuManager_SetRoomName_mBF3773CFAD794A9A5301F6522914D2C2715D69B3 (void);
// 0x00000055 System.Void MenuManager::PlayGame()
extern void MenuManager_PlayGame_m23E5A585A75D36A196E77133BBBE1E33ACB675BA (void);
// 0x00000056 System.Void MenuManager::QuitGame()
extern void MenuManager_QuitGame_m181685808151B2087C84F8B6F3C4D7D8E1F807D3 (void);
// 0x00000057 System.Void MenuManager::MainMenu()
extern void MenuManager_MainMenu_m8A559982BA05D1E0CF6367AF76F08B337CD31581 (void);
// 0x00000058 System.Void MenuManager::.ctor()
extern void MenuManager__ctor_m8F61CC885B72291B54C1C6EC368AE303EA856529 (void);
// 0x00000059 System.Void PauseManager::Update()
extern void PauseManager_Update_m2C800AF811359C3E56E97FC30FF2CAA99995E337 (void);
// 0x0000005A System.Void PauseManager::.ctor()
extern void PauseManager__ctor_m545310BC21D8C0D70A026984679E2439BC4B92FF (void);
// 0x0000005B System.Void PlayerMovement::Start()
extern void PlayerMovement_Start_mB585552228B1908E44D3A69496598FB485F608B6 (void);
// 0x0000005C System.Void PlayerMovement::Update()
extern void PlayerMovement_Update_mC3491BD6CDFF1FA543B16969144C939B2298052F (void);
// 0x0000005D System.Void PlayerMovement::FixedUpdate()
extern void PlayerMovement_FixedUpdate_m774280268A537B6ED9D9171CEAE67E9A0C3A9499 (void);
// 0x0000005E System.Void PlayerMovement::MovePlayer(System.Boolean)
extern void PlayerMovement_MovePlayer_m9D3657A0A3A3635D0333925E0E3A7AD0BA15A619 (void);
// 0x0000005F System.Void PlayerMovement::Crouch()
extern void PlayerMovement_Crouch_m67FACB43F40A6D301ECA5A35EDBEA374DD67A65C (void);
// 0x00000060 System.Void PlayerMovement::Sprint()
extern void PlayerMovement_Sprint_m98A3640CC41E9CECD59CAE4984DC0D1E2F915F63 (void);
// 0x00000061 System.Void PlayerMovement::RaycastCeilingChecks()
extern void PlayerMovement_RaycastCeilingChecks_m1FA1BFECFF261B1C2483D6E01D4692047FCF21A5 (void);
// 0x00000062 System.Void PlayerMovement::OnTriggerStay(UnityEngine.Collider)
extern void PlayerMovement_OnTriggerStay_mE6899483D4AC8168E4FF402DAEE1951FA6BF8E4D (void);
// 0x00000063 System.Void PlayerMovement::.ctor()
extern void PlayerMovement__ctor_mBF9F632DD9929DD6FF092A968649A4406BFE397F (void);
// 0x00000064 System.Void SensitivityController::Update()
extern void SensitivityController_Update_m7340EDB7E947EC2AE73E4DF4C193901B43318746 (void);
// 0x00000065 System.Void SensitivityController::.ctor()
extern void SensitivityController__ctor_m5195B36FCCB415B28AA53C28178964229D4A96CC (void);
static Il2CppMethodPointer s_methodPointers[101] = 
{
	Letter__ctor_mAD96D012C22B8DF35C6D036250A8F947A827DAB7,
	LetterLoader_SetLetter_m8888CCD20911156DE5BB5292518EB9D30C176EB1,
	LetterLoader__ctor_mC632C041813FE27B9C3FAB0EBDA3AE9D3E029B89,
	CameraMovement_Start_mCBBF91D02D609BDE618205B77F07B1FFD53C0A41,
	CameraMovement_Update_m02A5D16421ED8C060C06A72144FEA9250C55CE2B,
	CameraMovement_SetSensivity_m0FBA41D5DC333A5B60BFD5CC290DA6F0B81DFA42,
	CameraMovement_CameraLookRotation_m68A00AC80F30083F65C63BF47119418A64A7F8E7,
	CameraMovement_QuickLookBack_m561AEE70EB4F49EDAEBC67AC324848E1D90B4384,
	CameraMovement_SetCursorState_m1008B826218BB7C58CD7C23D6D750B124ED1A8B7,
	CameraMovement_Interacting_mA6DB1B5CD7FDBA065DFE1E2657913355BE13D8CB,
	CameraMovement__ctor_mD0F05084B2475AA8C0A35AFB6E4C88D0D6ACEAAA,
	CameraMovement__cctor_m16070BB162FA3D85BBBFF01B728D48115758A28B,
	EnemyMovement_Start_mB666BB660A59344BCAE9FC29700BDE66E1923C8F,
	EnemyMovement_Update_mDC79756DF8371F3CEDD9D33F9D7E0734ABCD141C,
	EnemyMovement_FixedUpdate_m5986E4E1AAFE4FE79575DE73332CE5668D30A89C,
	EnemyMovement_Vision_mDE352EEACF474A852C3C3BD7E61FA82A163D341E,
	EnemyMovement_OnTriggerEnter_mEA3DD86A39F4BC94349C4F54DD252253B5D79C3F,
	EnemyMovement__ctor_mFD697CA9B04655253B087314FE67A3B388E55EFF,
	U3CU3Ec__cctor_m83BFA3B9CDB16819CA6A68533EC476613E6E4111,
	U3CU3Ec__ctor_m1EBDD2D597505E663934614FDF24DAC45C75515C,
	U3CU3Ec_U3CVisionU3Eb__19_0_m7C4A64AA0BF79D1DA3F8E4844F83EDC95A64C92E,
	GameManager_adjustHealth_m038AB8D4112F7AE16280E4DD404E510BFE5D4144,
	GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E,
	GameManager_SetActions_m98BE50DA36A667923298F5468BF3A6F344240DCA,
	GameManager_CallAction_mA8016A2AD19D1696066B5003E00D497C9605FBC5,
	GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1,
	GameManager_PickUpFunction_m35D731F93625BE2D10485AC4787071F621F8F14A,
	GameManager_HoldItem_m587391581B39D91DAE01A7AFEA45A70243C9EC4D,
	GameManager_PutItemBack_m67BC44FF52522AB0B8D1F76831022C1D844F1C6B,
	GameManager_Interact_mC745154B5424A165E17008ACC5837FBB411A7B3B,
	GameManager_PlayPiano_mA380B3243C66CD2FFF2D47CAF5B56C442AC1D36C,
	GameManager_AddToAnthology_mD755AE158B66AB6764FCB6E0320B6D773C8F8A02,
	GameManager_HideInItem_m8A5B7754FCF2A2FFBF88C86C5C418DE2879045C8,
	GameManager_ToggleLamp_m25A9EB49458FDD99E191AEB7323A324D79DB3B1E,
	GameManager_DisableLampLight_m89029C03CBAC60813CB8293FCCEF43E4BD0AD3E1,
	GameManager_JumpscareInteraction_m28AD084959417E11FFC07DB517E2EC337492FA7A,
	GameManager_CallJumpscare_m8DBAE3AFB1C7E3A95AE1F56562D4607A3DD587FC,
	GameManager_ToggleRoomLights_m8C4A2FFD276ABECD3BD5966ADFD6E825768C8E71,
	GameManager_EnterDoor_m8789CFFF0C99A2F5310776D91CD8B613168E85DF,
	GameManager_HandleUI_m7DD55CC987CD2122AA47673335C0D721CE122E5C,
	GameManager_SetInteractionUI_mAA4EB27271424B134062F7EE5AB5C502D7EFF4A4,
	GameManager_ScoreCounter_mB802BBC6D093A4A13C7E71B9DAEB426F92C6B176,
	GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D,
	U3CHoldItemU3Ed__25__ctor_m89F81FF8A4D3A0AD02A20E5EFC33E105AD97CF97,
	U3CHoldItemU3Ed__25_System_IDisposable_Dispose_m97BFB6D6E12339704505728CE2E771F929DAF1F6,
	U3CHoldItemU3Ed__25_MoveNext_m64842F109F3E5CB2FC95E90FC8A3348C9CEC0B23,
	U3CHoldItemU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD285725BA9E3166E630379865ACD8FCC7F11BE8B,
	U3CHoldItemU3Ed__25_System_Collections_IEnumerator_Reset_m69E913C1D3551E1352C4C4868717843372C83416,
	U3CHoldItemU3Ed__25_System_Collections_IEnumerator_get_Current_m9D8FB0DA25EAF4AEFE9945C9C921CDAE39C63FCD,
	U3CPutItemBackU3Ed__26__ctor_m8E1596EC73811FF47912D93AB594FA61E5AC6A9F,
	U3CPutItemBackU3Ed__26_System_IDisposable_Dispose_m1CC1DFD466C725508CB90A4E25602067C0017035,
	U3CPutItemBackU3Ed__26_MoveNext_m11A53FD679A7D3A898CE9434330D47B4C8BCAB66,
	U3CPutItemBackU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBB881D70FABD4D44B103EAECAAEC1777936FBC5F,
	U3CPutItemBackU3Ed__26_System_Collections_IEnumerator_Reset_m2381396556CD6BD919AC2ACFBD124A562FFC3F33,
	U3CPutItemBackU3Ed__26_System_Collections_IEnumerator_get_Current_m5E1352C3F93E1CEE02C83D58C1652FA90AAE1D40,
	U3CAddToAnthologyU3Ed__29__ctor_mE00A39568E7B11F4CDAE27497D178B22CAEEAACD,
	U3CAddToAnthologyU3Ed__29_System_IDisposable_Dispose_m62971ECBB5A34D558CB0169E7A61A331CC0063D4,
	U3CAddToAnthologyU3Ed__29_MoveNext_m996D2A46004239BCF88C14E0679AF37D3F7A69F5,
	U3CAddToAnthologyU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m01D148E1C6FDBC7D26E2E678164C2CC52DDF9864,
	U3CAddToAnthologyU3Ed__29_System_Collections_IEnumerator_Reset_mBB794F7C08E4CC7F6C0F0BC91EB48299CE2BBAF1,
	U3CAddToAnthologyU3Ed__29_System_Collections_IEnumerator_get_Current_mD0EA287B06953CC7A85BAB8F560DF8FEBBB67979,
	U3CCallJumpscareU3Ed__34__ctor_mF174E3341A734D551157AAD46F452BE8BC05C24E,
	U3CCallJumpscareU3Ed__34_System_IDisposable_Dispose_m312982AC4ACB7E28063DF29E743AB2384C22152F,
	U3CCallJumpscareU3Ed__34_MoveNext_mBBD45474A1C70902F9048CB23E5C1012353A5838,
	U3CCallJumpscareU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m47B314BD56C41DE7137B474CF6F328818691DA20,
	U3CCallJumpscareU3Ed__34_System_Collections_IEnumerator_Reset_mDAFF898253879A4405C9FC76B0225C2A55BBD809,
	U3CCallJumpscareU3Ed__34_System_Collections_IEnumerator_get_Current_m0719B040B8A8672404A13FBA7C78E35B69C3928A,
	Interactible_Start_m8C73B715550A3737AFCA1AE0F93F8B409F2731A0,
	Interactible_Update_m7539B2C9A1D4262E7439EAB6F405870489E3922A,
	Interactible_SelectFunction_m0C2C7D7A60026C7B4DD055A3AA265A345BC0B532,
	Interactible_GetAudioClip_m5D18C4DDCCEC81499CC48D3EEBF03E00546DC25E,
	Interactible__ctor_m1499DC3597B9DA9A50541636476CF3DF61A052A6,
	JumpTrigger_Start_mE3C8C936CBD174EC4C690E39401C68967001B548,
	JumpTrigger_OnTriggerEnter_m30E20C1E1003BFFF7DC8EFF4F6E7A37F1D83EE1F,
	JumpTrigger_EndJump_m09AD09B655B13D6AAD1D5DB254C5DF3CB4505C79,
	JumpTrigger__ctor_mA3CBA826B955A5F7ABD107AE87B82C02AC1E6614,
	U3CEndJumpU3Ed__8__ctor_mBFA9DAB81A880A4C12EBC3DD852C9479D66DB0F5,
	U3CEndJumpU3Ed__8_System_IDisposable_Dispose_m0FC917E3F6349A23718B9C6CCB439A5468530B81,
	U3CEndJumpU3Ed__8_MoveNext_m80B4AF45783CA3399C31E309D84B0C1E2435BAD3,
	U3CEndJumpU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m642084C8A35B9E4D690D79941AF724BAE0C24228,
	U3CEndJumpU3Ed__8_System_Collections_IEnumerator_Reset_mDA18F083097A989CEC48EF1DC9FB5213B460E75B,
	U3CEndJumpU3Ed__8_System_Collections_IEnumerator_get_Current_mEC8C80BD4CF225FE441CD1889401A1AE1B123A0B,
	MenuManager_Start_m4E4A5EF33C27448D7F34FD29B93589635F1B2EE2,
	MenuManager_SetRoomName_mBF3773CFAD794A9A5301F6522914D2C2715D69B3,
	MenuManager_PlayGame_m23E5A585A75D36A196E77133BBBE1E33ACB675BA,
	MenuManager_QuitGame_m181685808151B2087C84F8B6F3C4D7D8E1F807D3,
	MenuManager_MainMenu_m8A559982BA05D1E0CF6367AF76F08B337CD31581,
	MenuManager__ctor_m8F61CC885B72291B54C1C6EC368AE303EA856529,
	PauseManager_Update_m2C800AF811359C3E56E97FC30FF2CAA99995E337,
	PauseManager__ctor_m545310BC21D8C0D70A026984679E2439BC4B92FF,
	PlayerMovement_Start_mB585552228B1908E44D3A69496598FB485F608B6,
	PlayerMovement_Update_mC3491BD6CDFF1FA543B16969144C939B2298052F,
	PlayerMovement_FixedUpdate_m774280268A537B6ED9D9171CEAE67E9A0C3A9499,
	PlayerMovement_MovePlayer_m9D3657A0A3A3635D0333925E0E3A7AD0BA15A619,
	PlayerMovement_Crouch_m67FACB43F40A6D301ECA5A35EDBEA374DD67A65C,
	PlayerMovement_Sprint_m98A3640CC41E9CECD59CAE4984DC0D1E2F915F63,
	PlayerMovement_RaycastCeilingChecks_m1FA1BFECFF261B1C2483D6E01D4692047FCF21A5,
	PlayerMovement_OnTriggerStay_mE6899483D4AC8168E4FF402DAEE1951FA6BF8E4D,
	PlayerMovement__ctor_mBF9F632DD9929DD6FF092A968649A4406BFE397F,
	SensitivityController_Update_m7340EDB7E947EC2AE73E4DF4C193901B43318746,
	SensitivityController__ctor_m5195B36FCCB415B28AA53C28178964229D4A96CC,
};
static const int32_t s_InvokerIndices[101] = 
{
	1722,
	1433,
	1722,
	1722,
	1722,
	1722,
	1451,
	1451,
	1451,
	1451,
	1722,
	2641,
	1722,
	1722,
	1722,
	1722,
	1433,
	1722,
	2641,
	1722,
	1064,
	1421,
	1722,
	1722,
	812,
	1722,
	1722,
	449,
	271,
	1722,
	1722,
	1114,
	1722,
	1722,
	1722,
	1722,
	1678,
	1722,
	1722,
	1722,
	872,
	1421,
	1722,
	1421,
	1722,
	1700,
	1678,
	1722,
	1678,
	1421,
	1722,
	1700,
	1678,
	1722,
	1678,
	1421,
	1722,
	1700,
	1678,
	1722,
	1678,
	1421,
	1722,
	1700,
	1678,
	1722,
	1678,
	1722,
	1722,
	1722,
	1678,
	1722,
	1722,
	1433,
	1678,
	1722,
	1421,
	1722,
	1700,
	1678,
	1722,
	1678,
	1722,
	1433,
	1722,
	1722,
	1722,
	1722,
	1722,
	1722,
	1722,
	1722,
	1722,
	1451,
	1722,
	1722,
	1722,
	1433,
	1722,
	1722,
	1722,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	101,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
